import {computedFrom} from 'aurelia-framework';
export class Welcome {
  _fecha = new Date();
  constructor() {
  }

  @computedFrom('_fecha')
  get fecha() {
    // console.log(Date());
    return this._fecha;
  }

  attached() {
    this.setIntervalHandler = setInterval(()=>{
      this._fecha = new Date();
    }, 1000);
  }

  dettached() {
    this.setIntervalHandler.detach();
  }
}
