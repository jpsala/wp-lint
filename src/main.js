/*eslint-disable no-var,no-unused-vars*/
// var Promise = window.Promise;//|| require('bluebird').config({warnings: false});
var Promise = require('../node_modules/bluebird/js/browser/bluebird.min.js').config({warnings: false});
import { bootstrap } from 'aurelia-bootstrapper-webpack';

import 'bootstrap';

import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import '../node_modules/font-awesome/css/font-awesome.min.css';
import '../styles/styles.css';

bootstrap(function(aurelia) {
  aurelia.use
    .standardConfiguration()
    .developmentLogging()
    .feature('resources');

  aurelia.start().then(() => aurelia.setRoot('app', document.body));
});
