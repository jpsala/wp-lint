import moment from 'moment';
export class DateValueConverter {
  toView(value, format = 'D/M/YYYY h:mm:ss') {
    return moment(value).format(format);
  }
}
